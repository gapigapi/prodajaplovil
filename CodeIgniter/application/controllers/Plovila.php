<?php

class Plovila extends CI_Controller
{

    public function __construct()
    {
        parent::__construct();
	
        $this->load->model('boatsdo');
        $this->load->model('com');
        $this->load->helper('url_helper');
        $this->load->helper('form');
        $this->load->library('session');
		$this->load->library('upload');



    }

    public function index()
    {
        $data['boat'] = $this->boatsdo->get_boat();

        //$data['title'] = "Čolni";
        $this->load->view('Templates/Header', $data);
        $this->load->view('boats/index', $data);
        $this->load->view('Templates/footer');
    }

    public function view($slug = NULL)
    { 		
		$d=$this->boatsdo->getinfo($slug);
		$data['info']=$this->boatsdo->gettel($d['IDuser']);
		$data['boat'] = $this->boatsdo->get_boat($slug);
		$data['byboat'] = $this->boatsdo->get_byboat($slug);
		$_POST['ID']=$data['boat']['ID'];
        $data['com'] = $this->com->get_com($slug);
		$data['user'] = $this->com->getusername($slug);
		$avg=0;
		foreach($data['com'] as $co)
			$avg+=$co['Ocena'];
			if($avg!=0)
			$avg=$avg/count($data['com']);
			$data['avg']=$avg;
		
		
		 $this->load->helper('form');
		 
        $this->load->view('Templates/Header');
        $this->load->view('boats/view', $data);
        $this->load->view('Templates/footer');
    }


    public function search()
    {
        $data['boat'] = $this->boatsdo->get_boata($_POST['Ime']);
		$data['byboat'] = $this->boatsdo->get_byboat($data['boat']['ID']);
		$_POST['ID']=$data['boat']['ID'];
        $data['com'] = $this->com->get_com($data['boat']['ID']);
		$d=$this->boatsdo->getinfo($data['boat']['ID']);
		$data['info']=$this->boatsdo->gettel($d['IDuser']);
		$data['user'] = $this->com->getusername($data['boat']['ID']);
		$avg=0;
		foreach($data['com'] as $co)
			$avg+=$co['Ocena'];
			if($avg!=0)
			$avg=$avg/count($data['com']);
			$data['avg']=$avg;
		
		
        $data['title'] = "Boats";
        $this->load->view('Templates/Header');
        if (isset($data['boat']))
            $this->load->view('boats/view', $data);
        else
            $this->load->view('errors/index');
        $this->load->view('Templates/footer');
    }


    public function create()
    {
        if (!isset($this->session->userdata['logged_in'])) {
            $data['message_display'] = 'Signin to view this page!';
            $this->load->view('Templates/Header');
            $this->load->view('user_authentication/login_form', $data);
            $this->load->view('Templates/footer');
            return;
        }

        $this->load->helper('form');
        $this->load->library('form_validation');

        $data['title'] = 'Dodaj nov čoln';

        $this->form_validation->set_rules('Ime', 'Ime', 'required');
        $this->form_validation->set_rules('cena', 'Cena', 'required');
		
		

        if ($this->form_validation->run() === FALSE) {
            $this->load->view('Templates/Header', $data);
            $this->load->view('boats/create');
            $this->load->view('Templates/footer');

        } else {
		$config['upload_path'] = './pictures/';
        $config['allowed_types'] = 'gif|jpg|png';
        //$config['max_size'] = 0;
        //$config['max_width'] = 1500;
        //$config['max_height'] = 1500;
		$this->load->library('upload', $config);
		$this->upload->initialize($config);
			
			
			if (!$this->upload->do_upload('slika')) {
            $error = array('error' => $this->upload->display_errors());
			print_r($error);
        } else {
            $data1 = array('image' => $this->upload->data());
			print_r($data,"dela");
			echo $this->upload->data('file_name');
        }
		$location="pictures/".$this->upload->data('file_name');
			//$this->load->view('boats/upload',$data);
			        $data = array(
            'Ime' => $this->input->post('Ime'),
            'Znamka' => $this->input->post('Znamka'),
            'Registracija' => $this->input->post('register'),
            'letnik' => $this->input->post('letnik'),
            'dolžina' => $this->input->post('dolg'),
            'moč' => $this->input->post('moc'),
            'spalnice' => $this->input->post('spal'),
            'tla' => $this->input->post('tla'),
            'streha' => $this->input->post('streha'),
            'servisi' => $this->input->post('servis'),
            'teža' => $this->input->post('teza'),
            'voda' => $this->input->post('voda'),
            'gorivo' => $this->input->post('gorivo'),
            'potniki' => $this->input->post('potnik'),
            'kategorija' => $this->input->post('kat'),
            'garaža' => $this->input->post('garage'),
            'na_voljo' => $this->input->post('voljo'),
            'cena' => $this->input->post('cena'),
            'Povzetek_Novinci' => $this->input->post('povzetek'),
			'slika'=>$location
        );
		
		 $result = $this->boatsdo->poglej($data);
            if ($result == TRUE) {
			$id['boat']=$this->boatsdo->get_id();
            $this->load->view('Templates/Header');
            $this->load->view('boats/success',$data);
            $this->load->view('Templates/footer');
            $this->boatsdo->setproda($this->session->userdata['logged_in']['IDkupec'],$id['boat']['ID']);

            } else {
                $this->load->view('Templates/Header');
                $this->load->view('boats/create');
                $this->load->view('Templates/footer');
            }
		
			
        }
    }


    public function delete($slug = NULL)
    {
        if (!isset($this->session->userdata['logged_in'])) {
            $data['message_display'] = 'Signin to view this page!';
            $this->load->view('Templates/Header');
            $this->load->view('user_authentication/login_form', $data);
            $this->load->view('Templates/footer');
            return;
        }
        $data['title'] = "Boats";
        $this->boatsdo->delete_boat($slug);
        $this->load->view('Templates/Header', $data);
        $this->load->view('boats/success', $data);
        $this->load->view('Templates/footer');
    }


    public function update($slug = NULL)
    {
        if (!isset($this->session->userdata['logged_in'])) {
            $data['message_display'] = 'Signin to view this page!';
            $this->load->view('Templates/Header');
            $this->load->view('user_authentication/login_form', $data);
            $this->load->view('Templates/footer');
            return;
        }
        $this->load->helper('form');
        $this->load->library('form_validation');

        $data['boat'] = $this->boatsdo->get_boat($slug);
        $data['title'] = 'Update a boat';

        $this->form_validation->set_rules('Ime', 'Ime', 'required');
        $this->form_validation->set_rules('cena', 'Cena', 'required');

        if ($this->form_validation->run() === FALSE) {
            $this->load->view('Templates/Header', $data);
            $this->load->view('boats/edit', $data);
            $this->load->view('Templates/footer');

        } else {
            $d = array(
                'Ime' => $this->input->post('Ime'),
                'Znamka' => $this->input->post('Znamka'),
                'Registracija' => $this->input->post('register'),
                'letnik' => $this->input->post('letnik'),
                'dolžina' => $this->input->post('dolg'),
                'moč' => $this->input->post('moc'),
                'spalnice' => $this->input->post('spal'),
                'tla' => $this->input->post('tla'),
                'streha' => $this->input->post('streha'),
                'servisi' => $this->input->post('servis'),
                'teža' => $this->input->post('teza'),
                'voda' => $this->input->post('voda'),
                'gorivo' => $this->input->post('gorivo'),
                'potniki' => $this->input->post('potnik'),
                'kategorija' => $this->input->post('kat'),
                'garaža' => $this->input->post('garage'),
                'na_voljo' => $this->input->post('voljo'),
                'cena' => $this->input->post('cena'),
                'Povzetek_Novinci' => $this->input->post('povzetek'),
            );
            $this->boatsdo->update_boat($slug, $d);

            $this->load->view('Templates/Header', $data);
            $this->load->view('boats/success');
            $this->load->view('Templates/footer');
        }
    }


    public function addby($slug = NULL)
    {

        if (!isset($this->session->userdata['logged_in'])) {
            $data['message_display'] = 'Signin to view this page!';
            $this->load->view('Templates/Header');
            $this->load->view('user_authentication/login_form', $data);
            $this->load->view('Templates/footer');
            return;
        }

        $this->load->helper('form');
        $this->load->library('form_validation');

        $data['title'] = 'Dodaj nov byboat';
		print_r($slug);
		$sluga['slug']=$slug;

        $this->form_validation->set_rules('Ime', 'Ime', 'required');
        $this->form_validation->set_rules('doplačilo', 'doplačilo', 'required');

        if ($this->form_validation->run() === FALSE) {
            $this->load->view('Templates/Header', $data);
            $this->load->view('boats/createby', $sluga);
            $this->load->view('Templates/footer');

        } else {
			
			$config['upload_path'] = './pictures/';
			$config['allowed_types'] = 'gif|jpg|png';
			$this->load->library('upload', $config);
			$this->upload->initialize($config);
			
			if (!$this->upload->do_upload('slika')) {
            $error = array('error' => $this->upload->display_errors());
			print_r($error);
        } else {
            $data1 = array('image' => $this->upload->data());
			print_r($data,"dela");
			echo $this->upload->data('file_name');
        }
		$_POST['loc']="pictures/".$this->upload->data('file_name');
		
			
			
            $this->boatsdo->set_byboat($slug);
            $this->load->view('Templates/Header');
            $this->load->view('boats/success');
            $this->load->view('Templates/footer');
        }
    }

    public function addrating($slug = NULL)
    {
		$slug['idkupec']=$this->session->userdata['logged_in']['IDkupec'];
        $this->com->set_com($slug);
        $this->view($_POST['ID']);
    }
	
	 /*   public function statik()
    {
		$this->com->showpast();
		    $this->load->view('Templates/Header');
            $this->load->view('Templates/footer');
		
    }*/
	


}