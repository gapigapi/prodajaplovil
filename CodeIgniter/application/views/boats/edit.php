<?php echo validation_errors(); ?>

<?php echo form_open_multipart('plovila/update/' . $boat["ID"] ); ?>

 <div style="border: 1px solid black; background-color:white;">	

  <label for="Ime">Ime:</label>
    <input type="text" name="Ime" value='<?php echo $boat["Ime"]; ?>'/><br />

    <label for="Znamka">Znamka:</label>
	<input type="text" name="Znamka" value='<?php echo $boat["Znamka"]; ?>'/><br />
	
	<label for="register">Registracija:</label>
    <input type="text" name="register" value='<?php echo $boat["Registracija"]; ?>'/><br />

    <label for="letnik">Letnik:</label>
	<input type="date" name="letnik" max="<?= date('Y-m-d'); ?>" value='<?php echo $boat["Letnik"]; ?>' /><br />
	
	<label for="dolg">Dolžina v metrih:</label>
    <input type="text" name="dolg" size=10 value='<?php echo $boat["Dolžina"]; ?>' /><br />

    <label for="moc">Moč motorjev:</label>
	<input type="text" name="moc" min="0" value='<?php echo $boat["Moč"]; ?>'/><br />
	
	<label for="spal">Spalnice:</label>
    <input type="number" name="spal" min="0" value='<?php echo $boat["Spalnice"]; ?>'/><br />

    <label for="tla">tla:</label> <br>
  <input type="radio"  name="tla" value="les">
  <label for="tla">les</label><br>
  <input type="radio" name="tla" value="Plastika">
  <label for="tla">Plastika</label> <br/>

	<label for="streha">Streha: </label>
	<input list="streha" name="streha" value='<?php echo $boat["Streha"]; ?>'>
    <datalist id="streha" >
    <option value="Hardtop">
    <option value="cerade">
    <option value="Fly">
    <option value="Open">
  </datalist> <br>

    <label for="servis">Servis: </label> <br>
	<textarea name="servis"><?php echo $boat["Servisi"]; ?></textarea><br />
	
	<label for="teza">Teža: </label>
    <input type="number" name="teza" min="0" value='<?php echo $boat["Teža"]; ?>'/><br />

    <label for="voda">Rezervoar vode: </label>
	<input type="number" name="voda" min="0" value='<?php echo $boat["Voda"]; ?>' /><br />
	
	<label for="gorivo">Rezervoar goriva: </label>
    <input type="number" name="gorivo"  min="0" value='<?php echo $boat["Gorivo"]; ?>'/><br />

    <label for="potnik">Potniki: </label>
	<input type="number" name="potnik" min="0" value='<?php echo $boat["Potniki"]; ?>' /><br />
	
	<label for="kat">Kategorija: </label>
    <input type="text" name="kat" max="10"  value='<?php echo $boat["Kategorija"]; ?>'/><br />

    <label for="garage">Garaža:</label><br>
	  <input type="radio" name="garage" value="Yes">
  <label for="garage">Da</label><br>
  <input type="radio" name="garage" value="No">
  <label for="garage">Ne</label> <br>
  
      <label for="voljo">Na voljo:</label><br>
	  <input type="radio" name="voljo" value="Yes">
  <label for="voljo">Da</label><br>
  <input type="radio" name="voljo" value="No">
  <label for="voljo">Ne</label> <br>

     <label for="povzetek">OPIS: </label>
	<input type="text" name="povzetek"  value='<?php echo $boat["Povzetek_novinci"]; ?>'/><br />
	
    <label for="cena">Cena(€): </label>
	<input type="number" name="cena" min="1"  value='<?php echo $boat["Cena"]; ?>'/><br />
	
  <label for="slika">Dodaj slike:</label>
  <input type="file" id="myfile" name="slika" value='<?php echo $boat["slika"]; ?>'>	
  
   
	

    <input type="submit" name="submit" class="submit" value="Update boat" />

</div>
