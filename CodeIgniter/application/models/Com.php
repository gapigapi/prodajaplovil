<?php

class Com extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
    }

    public function get_com($slug = FALSE)
    {
		//print($slug);
        
        $query = $this->db->get_where('Mnenje', array('IDplovilo' => $_POST['ID']));
        return $query->result_array();
    }
	

    public function set_com($slug)
    {
        $this->load->helper('url');

        $sluga = url_title($this->input->post('ime'), 'dash', TRUE);
        $data = array(
			'IDkupec'=>$slug['idkupec'],
			'IDplovilo'=>$this->input->post('ID'),
			'Mnenje' => $this->input->post('mnenje'),
            'Ocena' => $this->input->post('rate')
        );
		//print_r($data);
		

        return $this->db->insert('Mnenje', $data);
    }

    public function delete_com($slug)
    {
        $this->db->where('IDplovilo', $slug);
        return $this->db->delete('Mnenje');
    }
	
	
	
    public function getusername($slug = FALSE)
    {
        //$sql = 'SELECT user_name FROM Uporabnik, Mnenje WHERE Uporabnik.ID = IDkupec and IDplovilo= '. $_POST['ID'];
			//$this->db->query($sql);
			$this->db->select('user_name');
			 $this->db->from('Uporabnik');
			  $this->db->join('Mnenje', 'Uporabnik.ID=Idkupec');
			   $this->db->where('IDplovilo', $_POST['ID']);
			$q= $this->db->get();
		//$query = $this->db->get_where('Uporabnik, Mnenje', array('IDplovilo' => $_POST['ID'], 'IDkupec'=>'Uporabnik.ID'));
			//print_r($q);
	   return $q->result_array();
        //return $sql->result_array();
    }
	
	


}