<?php

class Login_database extends CI_model
{
    public function __construct()
    {
        $this->load->database();
    }

// Insert registration data in database
    public function registration_insert($data)
    {

        // Query to check whether username already exist or not
        $condition = "user_name =" . "'" . $data['user_name'] . "'";
        $this->db->select('*');
        $this->db->from('Uporabnik');
        $this->db->where($condition);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 0) {

            // Query to insert data in database
            $this->db->insert('Uporabnik', $data);
            if ($this->db->affected_rows() > 0) {
                return true;
            }
        } else {
            return false;
        }
    }

// Read data using username and password
    public function login($data)
    {

        $condition = "user_name =" . "'" . $data['username'] . "' AND " . "user_password =" . "'" . $data['password'] . "'";
        $this->db->select('*');
        $this->db->from('Uporabnik');
        $this->db->where($condition);
        $this->db->limit(1);
        $query = $this->db->get();
		
		//$q=$query->result_array();
		//if(isset $q['0'])
		//$_POST['vpis']=$q['0']['ID'];
		
        if ($query->num_rows() == 1) {
            return true;
        } else {
            return false;
        }
    }

// Read data from database to show data in admin page
    public function read_user_information($username)
    {

        $condition = "user_name =" . "'" . $username . "'";
        $this->db->select('*');
        $this->db->from('Uporabnik');
        $this->db->where($condition);
        $this->db->limit(1);
        $query = $this->db->get();

        if ($query->num_rows() == 1) {
            return $query->result();
        } else {
            return false;
        }
    }
}

?>