
<?php
class News_model extends CI_Model {

        public function __construct()
        {
                $this->load->database();
        }
	public function get_news($slug = FALSE)
{
        if ($slug === FALSE)
        {
                $query = $this->db->get('Plovilo');
                return $query->result_array();
        }

        $query = $this->db->get_where('Plovilo', array('slug' => $slug));
        return $query->row_array();
}
public function set_news()
{
    $this->load->helper('url');

    $slug = url_title($this->input->post('ime'), 'dash', TRUE);

    $data = array(
        'ime' => $this->input->post('ime'),
        'slug' => $slug,
        'Znamka' => $this->input->post('znamka')
    );

    return $this->db->insert('news', $data);
}
public function delete_news($slug){
	$this->db->where('slug', $slug);
	return $this->db->delete('Plovilo');
}
public function update_news($slug,$data){
	$this->db->set($data);
	$this->db->where('slug', $slug);
	return $this->db->update('Plovilo');
}

	
}