<?php

class Boatsdo extends CI_Model
{

    public function __construct()
    {
        $this->load->database();
    }

    public function get_boat($slug = FALSE)
    {
        if ($slug === FALSE) {
            $query = $this->db->get('Plovilo');
            return $query->result_array();
        }

        $query = $this->db->get_where('Plovilo', array('ID' => $slug));
        return $query->row_array();
    }

    public function get_boata($slug = FALSE)
    {
        if ($slug === FALSE) {
            $query = $this->db->get('Plovilo');
            return $query->result_array();
        }

        $query = $this->db->get_where('Plovilo', array('Ime' => $slug));
        return $query->row_array();
    }

	    public function poglej($data)
    {

        // Query to check whether username already exist or not
        $condition = "Ime =" . "'" . $data['Ime'] . "'";
        $this->db->select('*');
        $this->db->from('Plovilo');
        $this->db->where($condition);
        $this->db->limit(1);
        $query = $this->db->get();
        if ($query->num_rows() == 0) {
			//print_r($_FILES);	
            // Query to insert data in database
            $this->db->insert('Plovilo', $data);
            if ($this->db->affected_rows() > 0) {
                return true;
            }
        } else {
            return false;
        }
    }
	

    public function delete_boat($slug)
    {
        $this->db->where('ID', $slug);
        return $this->db->delete('Plovilo');
    }

    public function update_boat($slug, $data)
    {
        $this->db->set($data);
        $this->db->where('ID', $slug);
        return $this->db->update('Plovilo');
    }


public function get_id()
    {
		//$sql2 ="SELECT ID from Plovilo where Registracija= ".$_POST['register'];
		//$this->db->query($sql2);
		
		 $query = $this->db->get_where('Plovilo', array('Registracija' => $_POST['register']));
        return $query->row_array();

	}
    public function setproda($a,$id)
    {
		 $this->load->helper('url');

        $slug = url_title($this->input->post('ime'), 'dash', TRUE);
        $pro = array(
            'Iduser' => $a,
            'IDplovilo' =>$id,
            'Začetna_cena' => $this->input->post('cena'));
        //print_r($pro);
        $this->db->insert('Prodajalec', $pro);
    }
	
    public function getinfo($slug)
    {

        $query = $this->db->get_where('Prodajalec', array('IDplovilo' => $slug));
        return $query->row_array();
    }
	
	    public function gettel($slug)
    {
        $query = $this->db->get_where('Uporabnik', array('ID' => $slug));
        return $query->row_array();
    }




    public function set_byboat($sluga)
    {
        //print($sluga);
        $data = array(
            'Idplovilo' => $this->input->post('idplovilo'),
            'Ime' => $this->input->post('Ime'),
            'dolžina' => $this->input->post('dolg'),
            'moč_motorja' => $this->input->post('moc'),
            'material' => $this->input->post('tla'),
            'teža' => $this->input->post('teza'),
            'št_potnikov' => $this->input->post('potnik'),
            'doplačilo' => $this->input->post('doplačilo'),
			'slika'=>$_POST['loc']
        );

        return $this->db->insert('Byboat', $data);
    }
	
    public function get_byboat($slug = FALSE)
    {
        if ($slug === FALSE) {
            $query = $this->db->get('Byboat');
            return $query->result_array();
        }

        $query = $this->db->get_where('Byboat', array('IDplovilo' => $slug));
        return $query->row_array();
    }

}